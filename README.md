# Terraform ADRs

![license](https://img.shields.io/badge/license-CC0--1.0-brightgreen)

Architecture Decision Records for organising terraform code and CI/CD pipelines
