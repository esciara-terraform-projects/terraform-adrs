# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.2.0](https://gitlab.com/esciara-terraform-projects/terraform-adrs/compare/v0.1.0...v0.2.0) (2020-10-23)


### Features

* **adr:** [accepted] 0003-use-separate-terraform-state-buckets-for-each-environment ([fa9ebf4](https://gitlab.com/esciara-terraform-projects/terraform-adrs/commit/fa9ebf47397458a5a6c6b29e822238e41b684324))

## [0.1.0](https://gitlab.com/esciara-terraform-projects/terraform-adrs/compare/v0.0.0...v0.1.0) (2020-10-23)


### Features

* **adr:** [accepted] 0002-create-and-manage-state-of-state-buckets-through-code ([f11c234](https://gitlab.com/esciara-terraform-projects/terraform-adrs/commit/f11c234e76f3f472e5f2ecc708c20589290c8d98))
