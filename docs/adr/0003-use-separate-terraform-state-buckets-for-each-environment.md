# Use separate terraform state buckets for each environment

* Status: accepted
* Date: 2020-10-23

## Context and Problem Statement

State buckets for different environments (dev, staging, prod...) can be organised in all sorts of ways, from having them all in a single bucket to dedicating one bucket to each environment.

## Decision Drivers <!-- optional -->

* Security must be taken into account.

## Considered Options

* `Use one bucket per project, which contains all states for all environments`
* `Use separate buckets for each environment`, each bucket containing potentially several terraform state files, representing each separate parts of the infrastructure

## Decision Outcome

Chosen option: `Use separate buckets for each environment`, because:
* We want strong segregation to separate access privileges for each environment, with use of `uniform` bucket-level access (as [recommended by Google](https://cloud.google.com/storage/docs/access-control#choose_between_uniform_and_fine-grained_access))
* The cost is the same than having a single bucket
* The management cost is the same, as it is all handled through code
