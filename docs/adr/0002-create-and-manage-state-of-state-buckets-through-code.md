# Create and manage state of state buckets through code

* Status: accepted
* Date: 2020-10-22

## Context and Problem Statement

We want to host remote `terraform` states of our infrastructure on GCP buckets. We want to manage these buckets through code, which means that they themselves will need a bucket hosting their remote state. The question is how to organise this.

## Decision Drivers <!-- optional -->

* We want minimal manual intervention, even through at least one is needed to get out of the "Chickent and egg" dilemma.

## Considered Options

* `Manual creation and management`: the state of states bucket is created and managed manually only, i.e. not through code.
* `Use state of states bucket to host its own state, with configuration safeguard`: prevent accidental deletion by keeping `force_destroy=false`.
* `Use state of states bucket to host its own state, with permission safeguard`: prevent accidental deletion by keeping `force_destroy=false` and restricting deletion permission to specific accounts/service accounts

## Decision Outcome

Chosen option: `Use state of states bucket to host its own state, with configuration safeguard`, because:
* Bucket is entirely managed through code, with the use of a local state first, then the transfer of that state to the bucket.
* For our usage, the `force_destroy=false` (which prevents deletion if bucket is not empty) will be enough, even though bigger organisation might - and probably should - have an extra layer of security through specific access to deletion permission.
